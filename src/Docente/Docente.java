package Docente;

public class Docente {
    private float numHijos;
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoBase;
    private float horas;
    
    public Docente(){
        this.numHijos=0.0f;
        this.numDocente=0;
        this.domicilio="";
        this.nombre="";
        this.nivel=0;
        this.pagoBase=0.0f;
        this.horas=0.0f;
    }
    
    public Docente(float numHijos,int numDocente,String nombre,String domicilio,int nivel,float pagoBase,float horas){
        this.numHijos=numHijos;
        this.numDocente=numDocente;
        this.domicilio=domicilio;
        this.nombre=nombre;
        this.nivel=nivel;
        this.pagoBase=pagoBase;
        this.horas=horas;
    }
    
    public Docente(Docente obj){
        this.numHijos=obj.numHijos;
        this.numDocente=obj.numDocente;
        this.domicilio=obj.domicilio;
        this.nombre=obj.nombre;
        this.nivel=obj.nivel;
        this.pagoBase=obj.pagoBase;
        this.horas=obj.horas;
    }

    public float getNumHijos() {
        return numHijos;
    }

    public void setNumHijos(float numHijos) {
        this.numHijos = numHijos;
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }

    public float getHoras() {
        return horas;
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }
    public float calcuPago(float pagoT ){
      if (nivel==1){
        pagoT=this.pagoBase*1.30f;
        pagoT=pagoT*this.horas;
      }  
      if (nivel==2){
        pagoT=this.pagoBase*1.50f;
        pagoT=pagoT*this.horas;
      }  
      if (nivel==3){
        pagoT=this.pagoBase*2f;
        pagoT=pagoT*this.horas;
      }  
      return pagoT;
    }
    public float calcuImpuesto(float impuesto,float pagoT){
        impuesto=pagoT*0.16f;
        pagoT=pagoT-impuesto;
        return impuesto;
    }
    public float calcuBono(int numHijos,float bono){
      if (numHijos==1 || numHijos==2){
        bono=this.pagoBase*0.5f;
      }  
      if (numHijos<6 && numHijos>3){
        bono=this.pagoBase*0.10f;
      }  
      if (nivel>5){
        bono=this.pagoBase*0.20f;
      }  
      return bono;
    }
    public float pagoTotal(float pagoT,float bono, float impuesto){
        return pagoT=pagoT+bono-impuesto;
    }
    
    
}
