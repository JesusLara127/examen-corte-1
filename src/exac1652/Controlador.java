package exac1652;

import Docente.Docente;
import Vista.dlgVista;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener{
    private Docente doc;
    private dlgVista vista;
    
    public Controlador(Docente doc,dlgVista vista){
        this.doc=doc;
        this.vista=vista;
        
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.cmbNivel.addActionListener(this);
        //vista.spnHijos.addActionListener(this);
    }
    public void iniciarVista(){
        this.vista.setTitle("DOCENTE");
        this.vista.setSize(781, 408);
        this.vista.setVisible(true);
    }
    
    public void limpiar(){
        vista.txtBono.setText("");
        vista.txtDomicilio.setText("");
        vista.txtHoras.setText("");
        vista.txtImpuesto.setText("");
        vista.txtNombre.setText("");
        vista.txtNumDocente.setText("");
        vista.txtPagoBase.setText("");
        vista.txtPagoHoras.setText("");
        vista.txtPagoTotal.setText("");
        vista.cmbNivel.setSelectedItem("select");
    }
    
    public static void main(String[] args) {
        Docente obj=new Docente();
        dlgVista vista=new dlgVista(new JFrame(),true);
        Controlador controlador=new Controlador(obj,vista);
        controlador.iniciarVista();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
    }
    
}
